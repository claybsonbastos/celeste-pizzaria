<?php
	get_header();
?>

	<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
	?>
				<section class="content">
					<div class="container">
						<div class="row">
							<div class="content__info">
								<h2 class="content__info--head"><?php the_title();?></h2>
								<p class="content__info--description">
									<?php 
										$content = get_the_content();
										$content = strip_tags($content);
										echo $content;
									?>
								</p>
							</div>
							<div class="content__gallery">
								<div class="row">
								<?php
									$imagens = get_field('imagens');
									for($i=0; $i<count($imagens);$i++){
										$imagem = $imagens[$i]['imagem'];
								?>
										<div class="col-6 col-md-4 col-lg-3">
											<div class="content__gallery--item">
												<figure>
													<a data-fancybox="gallery" href="<?php echo $imagem;?>""><img src="<?php echo $imagem;?>"></a>
												</figure>
											</div>
										</div>
								<?php
									}
								?>
								</div>
							</div>
						</div>
					</div>
				</section>
	<?php
			endwhile;
		else :
			echo wpautop( 'Sorry, no posts were found' );
		endif;
	?>

<?php
	get_footer();
?>