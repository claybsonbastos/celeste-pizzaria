<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<small class="footer__copy">&copy; Celeste Pizzaria, 2019</small>
			</div>
			
			<div class="col-md-3">
				<ul class="footer__social">
					<li class="footer__social--item">
						<a href="https://www.facebook.com/celestepizzaria/" target="_blank"><img src="<?php echo get_template_directory_uri();?>/dist/images/facebook.svg"></a>
					</li>
					<li class="footer__social--item">
						<a href="https://www.instagram.com/celestepizzariaoficial/" target="_blank"><img src="<?php echo get_template_directory_uri();?>/dist/images/instagram.svg"></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
</footer>
<?php wp_footer();?>
</body>
</html>