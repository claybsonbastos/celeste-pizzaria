<!DOCTYPE html>
<html lang="<?php language_attributes(); ?>">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php bloginfo('name');?> | <?php the_title();?></title>
	<?php wp_head();?>
</head>
<body <?php body_class(); ?>>
	<!-- header -->
	<header class="header">
		<div class="container">
			<div class="row">
				<div class="col-6">
					<?php the_custom_logo();?>
				</div>
				
				<div class="col-6">
					<a href="#toggle" class="hamburgler">
						<div class="bun top"></div>
						<div class="meat"></div>
						<div class="bun bottom"></div>
					</a>
					<div class="d-none d-lg-block">
						<?php wp_nav_menu('principal'); ?>
					</div>
				</div>
				<div class="d-lg-none">
					<?php wp_nav_menu('principal'); ?>
				</div>
			</div>
		</div>
	</header>
	
