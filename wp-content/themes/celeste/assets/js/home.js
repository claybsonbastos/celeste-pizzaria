$(document).ready(function() {
	slide();
        events();
        scroll();
});

function slide(){
	$('.featured .owl-carousel').owlCarousel({
		loop: true,
        nav: false,
        items:1,
        dots: true,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:false
	})
}

function scroll(){
        $('a[href^="#"]').on('click', function(e) {
                e.preventDefault();
                var id = $(this).attr('href'),
                                targetOffset = $(id).offset().top;
                                
                $('html, body').animate({ 
                        scrollTop: targetOffset - 100
                }, 500);
        });
}


function events(){
        $('.events .owl-carousel').owlCarousel({
                loop: true,
                nav: false,
                items:1,
                dots: true,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:false,
                responsive: {
                        768: {
                               items: 2,
                               margin: 30
                        },
                        1024: {
                               items: 3,
                               margin: 30
                        },
                }
        })
}
