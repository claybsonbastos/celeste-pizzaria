<?php

	// ATIVAR MENUS
	function register_menus() {
	  register_nav_menus(
	    array(
	      'principal' => __('Menu Principal'),
	    )
	  );
	}
	add_action( 'init', 'register_menus' );
	
	// PERMITIR EDIÇÃO DE MENUS PELOS EDITORES
	$role_object = get_role( 'editor' );
	$role_object->add_cap( 'edit_theme_options' );

	// ATIVAR IMAGENS DESTACADAS
	add_theme_support( 'post-thumbnails' );
	add_theme_support("custom-logo"); 

	// CARREGAR ESTILOS E SCRIPTS
	function add_assets() {
		wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/dist/css/libs/bootstrap.min.css', 'all');
		wp_enqueue_style('main-css', get_template_directory_uri() . '/dist/css/main.css', 'all');

		wp_enqueue_script('jquery-js', get_template_directory_uri() . '/dist/js/jquery.min.js', true);
		wp_enqueue_script('main-js', get_template_directory_uri() . '/dist/js/main-min.js', array('jquery-js'), '1.0', true);

		if(is_page('home')){
			wp_enqueue_style('owl-carousel-css', get_template_directory_uri() . '/dist/css/libs/owl.carousel.css', 'all');
			wp_enqueue_style('home-css', get_template_directory_uri() . '/dist/css/home.css', 'all');
			wp_enqueue_script('owl.carousel-js', get_template_directory_uri() . '/dist/js/owl.carousel-min.js', array('jquery-js'), '1.0', true);
			wp_enqueue_script('home-js', get_template_directory_uri() . '/dist/js/home-min.js', array('jquery-js'), '1.0', true);
		}else if(is_page('eventos')){
			wp_enqueue_style('eventos-css', get_template_directory_uri() . '/dist/css/eventos.css', 'all');
			
		}else if(is_single()){
			wp_enqueue_style('single-css', get_template_directory_uri() . '/dist/css/single.css', 'all');
			wp_enqueue_style('jquery.fancybox.min-css', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css', 'all');
			wp_enqueue_script('jquery.fancybox.min-js', get_template_directory_uri() . '/dist/js/jquery.fancybox.min.js', array('jquery-js'), '1.0', true);
		}

	}
	add_action( 'wp_enqueue_scripts', 'add_assets' );


?>