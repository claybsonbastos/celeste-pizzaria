<?php
	get_header();
	//Template name: Home
?>

<!-- featured -->
<section class="featured" style="background-image: url(<?php echo get_template_directory_uri();?>/dist/images/bg-menu.jpg);">
	<!-- <div class="owl-carousel">
		<div class="featured__item" style="background-image: url(<?php echo get_template_directory_uri();?>/dist/images/bg-featured.jpg);">
			<div class="container">
				<div class="row">
				</div>
			</div>
		</div>

		<div class="featured__item" style="background-image: url(<?php echo get_template_directory_uri();?>/dist/images/bg-featured.jpg);">
			<div class="container">
				<div class="row">
				</div>
			</div>
		</div>

		<div class="featured__item" style="background-image: url(<?php echo get_template_directory_uri();?>/dist/images/bg-featured.jpg);">
			<div class="container">
				<div class="row">
				</div>
			</div>
		</div>
	</div> -->
</section>

<!-- destak -->
<section class="destak" >
	<div class="destak__info">
		<h3 class="destak__info--head">A melhor pizza da região</h3>
		<span class="destak__info--description">Diariamente das 18:00 às 22:00</span>
	</div>
</section>

<!-- menu -->
<section class="menu_content" id="cardapio">
	<div class="container">
		<div class="row">
			<h2 class="menu_content__head">Pizzas Tradicionais</h2>
			<?php
				$cardapio = get_field('cardapio');
				for($i=0;$i<count($cardapio);$i++){
					$sabor = $cardapio[$i]['sabor'];
					$descricao = $cardapio[$i]['descricao'];
			?>
					<div class="col-md-6">
						<div class="menu_content__item">
							<h3 class="menu_content__item--head"><?php echo $sabor;?></h3>
							<span class="menu_content__item--description">
								<?php echo $descricao;?>
							</span>
						</div>
					</div>
			<?php
				}
			?>
		</div>
	</div>
</section>

<!-- events -->
<section class="events">
	<div class="container">
		<div class="row">
			<h2 class="events__head">Últimos Eventos</h2>
			<div class="owl-carousel">
			<?php 
                $paged = get_query_var('paged');
				$args = array(
					'post_type' => array('evento'),
					'posts_per_page' => -1,
		            'orderby' => 'DESC',
		            'order' => 'DESC',
		            'paged' => $paged
				);
				
				$the_query = new WP_Query( $args );
				if ($the_query->have_posts()):
					while ($the_query->have_posts()) : $the_query->the_post(); 
					$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
					$imagens = get_field('imagens');
					$data = get_field('data_do_evento');
					$content = get_the_content();
					$content = strip_tags($content);
            ?>
					<div class="events__item">
						<div class="events__item--image" style="background-image: url(<?php echo $imagens[0]['imagem'];?>)"></div>
						<div class="events__info">
							<span class="events__info--date"><?php echo $data;?></span>
							<a href="<?php the_permalink();?>">
								<h3 class="events__info--head"><?php the_title();?></h3>
							</a>
							<span class="events__info--description">
								<?php
									echo substr($content, 0, 100);  
	                                if (strlen($content) > 100) {
	                                    echo '...';
	                                }
								?>
							</span>
						</div>
					</div>

            <?php endwhile; endif; ?>
            </div>
		</div>
	</div>
</section>

<?php 
	if(have_rows('contato', 7)):
		while (have_rows('contato', 7)) : the_row();
?>
<!-- contact -->
<section class="contact" id="contato">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 d-lg-flex align-items-lg-center justify-content-lg-center">
				<address class="contact__info">
					<h3 class="contact__info--head"><?php the_sub_field('titulo');?></h3>
					<div class="contact__info--address"><?php the_sub_field('endereco');?></div>
					<a href="tel:<?php the_sub_field('telefone');?>" class="contact__info--phone"><?php the_sub_field('telefone');?></a>
					<a href="https://api.whatsapp.com/send?phone=5581998269159" class="contact__info--whatsapp" target="_blank"><?php the_sub_field('whatsapp');?></a>
				</address>
			</div>
			<div class="col-lg-6">
				<?php echo do_shortcode('[contact-form-7 id="36" title="Contato"]');?>
			</div>
		</div>
	</div>
</section>
<?php endwhile; endif;?>

<!-- map -->
<section class="map">
	<iframe src="https://snazzymaps.com/embed/166423" width="100%" height="600px" style="border:none;"></iframe>
</section>


<?php 
	get_footer(); 
?>