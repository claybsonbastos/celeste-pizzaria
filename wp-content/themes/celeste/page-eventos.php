<?php 
	get_header(); 
	//Template name: Eventos
?>

<section class="events">
	<div class="container">
		<div class="row">
			<h2 class="events__head"><?php the_title();?></h2>
			<?php 
                $paged = get_query_var('paged');
				$args = array(
					'post_type' => array('evento'),
					'posts_per_page' => -1,
		            'orderby' => 'DESC',
		            'order' => 'DESC',
		            'paged' => $paged
				);
				
				$the_query = new WP_Query( $args );
				if ($the_query->have_posts()):
					while ($the_query->have_posts()) : $the_query->the_post(); 
					$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
					$imagens = get_field('imagens');
					$data = get_field('data_do_evento');
					$content = get_the_content();
					$content = strip_tags($content);
            ?>
            		<div class="col-md-6 col-lg-4">
						<div class="events__item">
							<div class="events__item--image" style="background-image: url(<?php echo $imagens[0]['imagem'];?>)">
							</div>
							<div class="events__info">
								<span class="events__info--date"><?php echo $data;?></span>
								<a href="<?php the_permalink();?>">
									<h3 class="events__info--head"><?php the_title();?></h3>
								</a>
								<span class="events__info--description">
									<?php
										echo substr($content, 0, 100);  
		                                if (strlen($content) > 100) {
		                                    echo '...';
		                                }
									?>
								</span>
							</div>
						</div>
					</div>
            <?php endwhile; endif; ?>
        </div>
	</div>
</section>

<?php 
	get_footer(); 
?>