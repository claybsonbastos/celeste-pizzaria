var gulp        = require("gulp"),
    uglify      = require("gulp-uglify"),
    sass        = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat      = require("gulp-concat"),
    htmlmin     = require("gulp-htmlmin"),
    path        = require("path"),
    imagemin    = require("gulp-imagemin"),
    fileinclude = require("gulp-file-include"),
    minify = require('gulp-minify'),
    browserSync = require("browser-sync").create();

function swallowError(error) {
  console.log(error.toString());
  this.emit("end");
}

// OTIMIZANDO IMAGENS
gulp.task("images", function() {
  gulp.src("wp-content/themes/celeste/assets/images/**")
  .pipe(imagemin())
  .pipe(gulp.dest("wp-content/themes/celeste/dist/images"));
});

//MINIFY JS
gulp.task('compress', function() {
  gulp.src(['wp-content/themes/celeste/assets/js/**/*.js'])
    .pipe(minify())
    .pipe(gulp.dest('wp-content/themes/celeste/dist/js'))
});

// Funçao para compilar o SASS e adicionar os prefixos
function compilaSass() {
  return gulp
  .src(['wp-content/themes/celeste/assets/css/**/*.scss', 'wp-content/themes/celeste/assets/css/**/*.css' ])
  .pipe(sass({
    outputStyle: 'compressed'
  }))
  .pipe(autoprefixer({
    browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(gulp.dest('wp-content/themes/celeste/dist/css'))
  .pipe(browserSync.stream());
}

// Tarefa de gulp para a função de SASS
gulp.task('sass', function(done){
  compilaSass();
  done();
});

// RELOAD E INCLUDE
gulp.task("fileinclude", function() {
  gulp.src(["wp-content/themes/celeste/assets/css/**/*.scss"])
  .pipe(fileinclude({prefix: "@@", basepath: "@root"}))
  .pipe(gulp.dest("wp-content/themes/celeste/dist/css"))
  .pipe(browserSync.stream());
});


// SERVIDOR
gulp.task('browserSync', function() {
  //watch files
    var files = [
    './wp-content/themes/celeste/assets/css/**/*.scss',
    './wp-content/themes/celeste/*.php',
    './wp-content/themes/celeste/assets/js/**/*.js'
    ];

  browserSync.init(files,{
      proxy: "localhost:8080/celestepizzaria/"
  }); 
});

// WATCH LESS, SCRIPTS E LIVERELOAD
gulp.task("watch", function() {
  gulp.watch("wp-content/themes/celeste/**/*.php", ["fileinclude"]);
  gulp.watch("wp-content/themes/celeste/assets/js/**/*.js", ["compress"]);
  gulp.watch("wp-content/themes/celeste/assets/css/**/*.scss", ["sass"]);
});

gulp.task("default", ["fileinclude", "compress", "sass", "images", "browserSync", "watch"]);
